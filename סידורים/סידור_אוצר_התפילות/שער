% סדור אוצר התפלות

הברכות וההודאות, התשבחות והתהלות שתקנום לנו *הזקנים והנביאים (אנשי כנסת הגדולה),* ואחריהם *רבותינו שבא"י ושבבבל*. למן *עזרא הסופר וסיעתו* עד *גדולי רבותינו* שבדורות האחרונים, ועליהם נתלו המאורות הגדולים שכבר הופיע אורם עליהם, ורבים שהיו עד עתה ספוני טמוני *גנזי מרומים* ואורם הגנוז טרם יצאו מנרתיקם, ה״ה:

%% שני סדורים כתבי יד

א) ערוגת הבושם 

ב) ראשי בשמים

יכילו:

א) *עץ יוסף* באור המלות וקשור המשך המאמרים וחבורן, ותוכן הכוונות הפשטיות של כל המאמרים והענינים שנקבצו בסדור הזה, כלם בקצור נמרץ ע״פ הפשט הנכון ועל יסודי דברי חז״ל. נלקט ונבחר מתמצית *מאתים בחירי המפרשים* הראשונים והאחרונים, ויהיו יחד לבאור נפלא. —

ב) *ענף יוסף* הערות מאירות עינים, ממאמרי חז״ל בתלמוד בבלי וירושלמי וממדרשים שונים וזוה״ק, ומספרי *גאוני ישראל קדמאי ובתראי*.—

ג) יערה *מקור ויסוד כל מנהג* הנהוג בישראל שבא זכרו בהסדור, ויפיץ אור בהיר עליהם ע״פ הלבוש ושארי ספה"ק. —

ד) *ארבעים פרקים* האוצרים בקרבם נדולות וראמות במעלות למוד התורה ומעשי המצות, וביותר בעניני עבודת ה׳ בתפלה ובברכות,בכוונותיהם והשפעותיהם הקדושות על רוח האדם, ובעניני היראה והאהבה והדביקות בה' בתפלה ובעבודת אלהים בכלל.
*ארבעים הפרקים* הנשגבים האלה  חשובים לספר נעלה מאד בפ״ע. — 

ה) *מאמרים גדולים* לפני כל ענין וענין בבאורי המצות והמנהגים, מקוריהם טעמיהם וכוונותיהם.—

*שני הסדורים האלה* חוברו ע"י הרה״ג המפורסם מוהר״ר *חנוך זונדל* בהר״ר *יוסף* ז״ל (מחבר הפירושים *עץ יוסף וענף יוסף ויד יוסף* על כל המדרשים ועל עין •עקב ועוד), אשר בחר לקט ואסף מתמצית דברי חז״ל בש״ס ומדרשים בזוה״ק ובתק״ז ומספרי יראים וספרי קבלה, והוסיף נופך משלו דברים נשגבים חוצבים להבות אש קודש בכל הענינים ההם. —


%% סדור כתב יד *עבודת הלב*

יכיל

א) *עיון תפלה* באורים נפלאים עדיות שבעדיות מבחירי המפרשים הקדמונים והאחרונים, ומספרים עתיקים יקירי המציאות וכתבי ידי הגאונים, כלם נוסדו על דברי חז"ל הנפוצים בכל התורה שבעל פה, ועל פי עומק הפשט לפי חקי הדקדוק וההגיון, חקרי השמות הנרדפים והשתוות הלשונות שבמקומות שונים, והערה מקור כל פסוק ומאמר בכה״ק ובכל ספרי חז"ל.

ב) *תקון תפלה* כולל: א)שנויי כל הנוסחאות השונות שעלו בסדורי התפלה למיום הוחל הסדור להכתב בישראל, נוסחת תלמוד בבלי, נוסחת ירושלמי. נוסחת *רב סעדיה גאון* ז״ל. נוסחת רב *עמרם גאון* ז״ל. נוסחת *הרי"ף, הרמב״ם והרא״ש.* נוסח׳ *מחזור ויטרי ומחזור לבני רומא*. נוסחת *הספרדים, התימנים ,והאיטלינים.* נוסחת *בני כפא וקראסוב.* נוסחת *האר"י ז״ל,* וכדומה. —

ג) *המקורים הראשונים* לכל התפלות והברכות ולכל הענינים שנאצרו באוצר הזה תולדותם, מקריהם, שמות יוצריהם וזמני יצירתם ומטרתם, הכל ממקורות ספרי קדמונינו ז״ל. —

ד) *מבוא גדול* בראש הסדור. כולל חקרי הרבה מקצעות שבתפלה, על יסודי התלמוד המדרשים והפוסקים.

ה) *הערות גדולות* לכל המקצעות שבסי׳ להפיץ אור גדול על ענינים רבים שהיו סבוכים ונבוכים בקורות התפלה. 

*כל אלה* חוברו יחדיו ע״י הרה״ג המובהק החכם המדקדק מר *אריה ליב ן' שלמה גורדון* ז״ל בירושלם תוב״ב (מחבר ס' בינת המקרא משפטי לשון עברית ועוד).

*כשמונה עשרה שנה* ישב על אוצרות גנזי הספרים והעמיק חקר לבחור ולברור משופרי דשופרי שבהן את כל הענינים הנ״ל, וימץ תמציתם, כדבורה זו שמוצצת מיץ הפרחים ופולטת דבשם, ויצר את הסדור הנפלא הזה.

ועל כל אלה נוספו עוד מבחרי הבאורים מן

*סדור דובר שלום* וספר *אחרית לשלום* (כ״י) מהרב הג״מ יצחק אליהו לנדא ז"ל מ"מ דפה ווילנא (מחבר ס׳ *ברורי המדות* על המכילתא, *כפלים לתושיה. לשמוע כלמודים* על עין יעקב, ועוד).

הבאורים יפיצו אור חדש על הרבה מאמרים שבסדור. ואורם זרוע על פני כל הסדור ומלבד הבאורים הכוללים הנ"ל שמשתרעים על פני כל הסדור הדפסנו *באורים פרטיים לחלקים שונים של הסדור* מגדולי גאוני ישראל, שכל אחד מהן קובע ברכה ותהלה לעצמו, ה"ה: 

[כתב יד] א) *פירוש רש״י* ז״ל לתפלות ראש השנה, ולתפלת נעילה, ולהושענות. עם *הגהות ממחזור ויטרי,* ועליהן הערות המחבר *עיון תפלה* הנ״ל. (וי״א שהפרושים המה מאחד קדמון בדורו של רש״י). 

ב) פירוש *מסדור כל בו* יוצר להפסקות ולשבת הגדול.

ג) *באור הגר״א* ז״ל לפרקי אבות, הוגה ונתקן ע״פ כי״ק.

ד) פירוש *מעשה נסים* להגדה של פסח, לרבינו יעקב מליסא ז״ל (מחבר ס' חוות דעת ונתיבות המשפט וכו').

ה) *באור הרוו"ה* ז״ל למערבית.

ו) *באור* להפיוט אזכרה שנות עולמים (במערבית ליל ראשון של פסח) להג״מ *משה טוביה* ז"ל אבד״ק האגוי. 

ז) *באור* להפיוט *אור יום הנף* (במערבית ליל ב׳ ש״פ) להג"מ *נח חיים צבי* ז״ל אבד״ק מגנצא והסבורג. 

ח) פירוש *באר מיכל* למערבית ליל ב' ש"פ. 

ט) פי' מסי' *אור לישרים* לקרוב״ץ לפורים ולסליחות ך' סיון. 

י) פירוש נפלא מאת *הריב"א* ז״ל (מסי׳ עבודת ישראל) לשיר היחוד.

גם יחנו מסביב לכל המאורות הגדולים הנ"ל ככבי אור:

*הערות נפלאות כתבי יד מהרבה חכמי התורה* אשר יפיצו אור גדול, אור חדש על פני מאמרים וענינים רבים שבסדור. 

% דינים

*דרך החיים* לרבינו *יעקב מליסא* ז״ל הנ״ל. הדפסנוהו *בשלמותו* כפי שנדפס בחיי הרב המחבר ז״ל בתוצאה שניה באלטונא תקצ"א. והוספנו בו מלואים מהג״מ *שלמה גנצפריך* ז״ל (מחבר *קצור ש״ע* ועוד).

[כתב יד] *הגהות רבינו עקיבא איגר* ז״ל על דה״ח. נכתבו בעצם כי"ק על גליון הסדור דרה״ה שנדפס באלטונא הנ"ל. ונוספו עליהם הערות מהג״מ *יהושע העשיל* *הלוי לעווין* ז״ל שאסף *מחדושי רעק"א* על או״ח.

*נמוקי הגרי"ב* להג״מ ישעיה ברלין ז״ל , נעתקו מגליון סדורו ומגליון מחזור כל בו.

וגולת הכותרת לכל אלה היא:

*פיוט מר׳ ינאי* (הנזכר בש"ס בבלי. ובירושלמי נקרא "יניי") לשבת הנדול (תחלתו אוני פטרי רחמתים), והוא *יקר המציאות מאד*.

[כתב יד] *פיוטי ר' אלעזר הקליר* ז״ל למוסף פ׳ זכור ופ׳ פרה. נמצא בכ״י ישן על קלף בירושלים תוב״ב.

*שתי בקשות לרב סעדיה גאון* ז״ל. בהן ישפוך האדם שיחו לפני ה' בהתעטף עליו רוחו יתודה בהכנעה רבה ויתנחם על חטאתיו, ונפשו תערוג אל האלהים, —

שתי המרגליות הטובות האלה יקירי המציאות מאד. והן נפלאות בתוכנן וסגנונן. והראב״ע ז"ל בפירושו לקהלת (ה׳ א') מפליג מאד בשבחן בדברו שמה קשות על ארבעה דברים קשים שנמצאים בפיוטי הקליר, ואמר בזה״ל: "*והגאון רב סעדיה* נשמר מאלה הארבעה דברים בבקשותיו השתים שלא חבר מחבר כמו הם. והן על לשון המקרא ודקדוק הלשון באין חידות ומשלים ולא דרש" עכ״ל.

*בקשה לרבינו בחיי הדיין הספרדי* ז״ל (בעל חובות הלבבות), מיקירי המציאות מאד! תוכנה, השתפכות הנפש וכלותה אל האלהים, וידוי גדולה וחרטה עצומה, וכניעתה הגדולה לפני ה׳, עם כוספה הנערץ לקרבת אלהים, ולרוממותה והודאתה לשמו הגדול. —

עוד נלוו עליהם

*תוכחות ושירי קודש*

א) *תוכחה לרבינו בחיי* הנ״ל. 

ב) תוכחה *להראב״ע* ז"ל. 

ג) *שיר קודש* לר' *שלמה ן' גבירול* ז״ל. 

ד) לר' *משה ן' עזרא* ז״ל. 

ה) לר׳ *יהודה הלוי* ז"ל.

ו) לר' *אברהם ן' עזרא* ז״ל. 

ז) לר' *ישראל נאגאר״ה* ז״ל (מחבר הזמר "יה רבון" וספר "זמירות ישראל").

מלבד אלה שכבר נדפסו בסדורים, כמו: 

ח) *למהר"ם אלשקר* ז"ל. 

ט) לר' *שמעיהו הספרדי* ז"ל וכדומה.

גם הצגנו: 

א) תורת השב מחטאיו. 

ב) מסירת מודעה לחולה. 

ג) וידוי לשכיב מרע (מהרמב״ן ז״ל). 

ד) וידוי הגדול להרחיד״א ז״ל. 

ה) סדר הלמוד לאבל וליום שמת בו אביו או אמו (יאהרצייט). 

ו) מפתחות כל פרקי המשניות ע״פ א״ב.

ולדקדוק יושר הקריאה הצגנו:

ציון בתבנית ככב * על כל אות שמנוקדת בשוא נע, ומתגים לפני התנועות שנגינתן מלעיל.

*הסדור הענקי הזה מלבד שהוא אוצר גדול לתפלות, הנה הוא יותר הרבה אוצר גדול לספרי למוד וחקרי הפירושים והכוונות הנוסחאות והמקורים הראשונים של כל ענין שבא בסדור הזה.*
